var express = require('express');
var router = express.Router();

/* POST to Add User Service */
router.post('/adduser', function(req, res) {
    console.log('[[[[req]]]]');
    console.log(req);

    // Set our internal DB variable
    var db = req.db;

    // Get our form values. These rely on the "name" attributes
    var Name = req.body.Name;
    var Surname= req.body.Surname;
    var City = req.body.City;
    var CreateDate = req.body.CreateDate;
    var Address =req.body.Address;
    var Phone = req.body.Phone;

    // Set our collection
    var collection = db.get('bazaa');

    // Submit to the DB
    collection.insert({
        "Name" : Name,
        "Surname" : Surname,
        "City" : City,
        "CreatDate" : CreateDate,
        "Address" : Address ,
        "Phone" : Phone
    }, function (err, doc) {
        if (err) {
            // If it failed, return error
            res.send("TProblem.");
        }
        else {
            // And forward to success page
            res.redirect("userlist");
        }
    });
});

module.exports = router