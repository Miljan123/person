import React from "react";
import reqwest from "reqwest";


export default class Form extends React.Component {
  state = {
    _id: "",
    Name: "",
    Surname: "",
    City: "",
    CreateDate: "",
    Address: "",
    Phone: "",
    Users: [],
  };

  change = e => {
    this.props.onChange({ [e.target.name]: e.target.value });
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  componentDidMount() {
    this.getUsers();
  }

  onInsert = e => {
    console.log('e');
    console.log(e);
    e.preventDefault();
 
    reqwest({
        url: 'http://127.0.0.1:80/adduser'
      , method: 'post'
      , data: { 
        Name : this.state.Name, 
        Surname: this.state.Surname,
        City: this.state.City,
        CreateDate: this.state.CreateDate,
        Address: this.state.Address,
        Phone: this.state.Phone
    }
      , crossOrigin: true
      , success: (resp) => {
          // qwery('#content').html(resp)
          console.log('resp');
          console.log(resp);
          this.getUsers();
        }
    })

  };

  onUpdate = (e) => {
    e.preventDefault();

      reqwest({
        url: 'http://127.0.0.1:80/users'
      , method: 'put'
      , data: { 
        _id: this.state._id,
        Name : this.state.Name, 
        Surname: this.state.Surname,
        City: this.state.City,
        CreateDate: this.state.CreateDate,
        Address: this.state.Address,
        Phone: this.state.Phone
    }
      , crossOrigin: true
      , success: (resp) => {
          // qwery('#content').html(resp)
          console.log('resp');
          console.log(resp);
          this.getUsers();
        }
    })

  }

  getUsers = () => {
    reqwest({
        url: 'http://127.0.0.1:80/users'
      , method: 'get'
      , crossOrigin: true
      , success: (resp) => {
          // qwery('#content').html(resp)
          console.log('resp');
          console.log(resp)
          this.setState({Users: resp})
        }
    })
  }

  deleteUser = (id) => {
    console.log(id);
    reqwest({
        url: `http://127.0.0.1:80/users/${id}`
      , method: 'delete'
      , crossOrigin: true
      , success: (resp) => {
          // qwery('#content').html(resp)
          console.log('resp');
          console.log(resp);
          this.getUsers();
        }
    })
  }




  createNew = () => {
    this.setState({
      _id: "",
      Name: "",
      Surname: "",
      City: "",
      CreateDate: "",
      Address: "",
      Phone: "",
    })
  }

  setUserToForm = (user) => {
    this.setState(user);
  }

  render() {
    return (
      <div>

      <form>
      
        <input type="text" 
          name="Name"
          placeholder="First name"
          value={this.state.Name}
          onChange={e => this.change(e)}
        />
        <br />
        <input
          name="Surname"
          placeholder="Surname"
          value={this.state.Surname}
          onChange={e => this.change(e)}
        />
        <br />
        <input
          name="City"
          placeholder="City"
          value={this.state.City}
          onChange={e => this.change(e)}
        />
        <br />
        
        <input
          name="Address"
          
          placeholder="Address"
          value={this.state.Address}
          onChange={e => this.change(e)}
        />
        <br />
        <input
          name="Phone"
          placeholder="Phone"
          value={this.state.Phone}
          onChange={e => this.change(e)}
        />
        <br />
        {this.state._id ? <button onClick={this.onUpdate.bind(this)}>Update</button>: <button onClick={this.onInsert.bind(this)}>Insert</button>}
        
        <hr/>
        
      </form>

      <div>All users, count       {this.state.Users.length}</div>
      <button onClick={this.createNew.bind(this)}>create new</button>
      <table className="test">
          <thead>
          <tr>
            <th>Ime</th>
            <th>Surname</th>
            <th>Grad</th>
            <th>Datum</th>
            <th>Adresa</th>
            <th>broj</th>
            <th></th>
            <th></th>
        </tr>
          </thead>
        <tbody>
        {this.state.Users.map((user, index) => 

          <tr key={index} >
            <td>{user.Name}</td>
            <td> {user.Surname}</td>
            <td>{user.City}</td>
            <td> {user.CreatedDate}</td>
            <td>{user.Address}</td>
            <td>{user.Phone}</td>
            <td><button id="izmijeni" onClick={this.setUserToForm.bind(this, user)}> izmijeni </button></td>
            <td><button id="obrisi" onClick={this.deleteUser.bind(this, user._id)}> Izbrisi </button></td>
       
          </tr>
            )}
        </tbody>
      </table>
      </div>



    );
  }
  
}
