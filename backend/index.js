let mongoose = require('mongoose');
// let routerCustom = require('./routes');
var express = require('express');

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  next();
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(80, function () {
  console.log('Example app listening on port 80!');
});


/*app.post('/adduser', function(req, res) {

    // Set our internal DB variable
    var db = req.db;

    // Get our form values. These rely on the "name" attributes
    var Name = req.body.Name;
    var Surname= req.body.Surname;
    var City = req.body.City;
    var CreateDate = req.body.CreateDate;
    var Address =req.body.Address;
    var Phone = req.body.Phone;

    // Set our collection
    var collection = db.get('bazaa');

    Submit to the DB
    collection.insert({
        "Name" : Name,
        "Surname" : Surname,
        "City" : City,
        "CreatSate" : CreateDate,
        "Address" : Address ,
        "Phone" : Phone
    }, function (err, doc) {
        if (err) {
            // If it failed, return error
            res.send("TProblem.");
        }
        else {
            // And forward to success page
            res.redirect("userlist");
        }
    });
});

*/

 mongoose.connect('mongodb://localhost/bazaa');
 let Schema = mongoose.Schema;

 let PersonSchema = new Schema(
{
 	Name : String ,
 	Surname :  String ,
 	City : String ,
 	CreatedDate : Date ,
 	Address : String ,
 	Phone : Number 

 }, { collection: 'person' });

 let Person = mongoose.model('person', PersonSchema);

app.post('/adduser', function(req, res) {
	console.log("[[[[[adduser]]]]]");
	console.log(req.body);



	 let miljan = new Person({
	  Name : req.body.Name,
	  Surname : req.body.Surname,
	   City : req.body.City,
	   CreatedDate : new Date,
	   
	  Address: req.body.Address,
	  Phone : req.body.Phone
	 });

	 miljan.save(function(err, doc) {
	 	console.log('[[[err]]]');
	 	console.log(err);
	 	console.log('doc');
	 	console.log(doc);
	   if (err) throw err;

	   console.log('User saved successfully!');
	   res.sendStatus(200)
	 });

})

app.get('/users', function(req, res) {
    console.log("[[[[[get users]]]]]");
    console.log(req.body);

    Person.find({}, function (err, users) {
        console.log("number of users returned",users.length)
        res.send(users);
    });

});

app.delete('/users/:userId', function(req, res) {
    console.log("[[[[[delete user ]]]]]" + req.params.userId);
    console.log(req);

    Person.remove({_id: req.params.userId}, function (err, users) {
        res.sendStatus(200); 
    });

});

app.put('/users', function(req, res) {
    console.log("[[[[[adduser]]]]]");
    console.log(req.body);


     Person.update({_id: req.body._id}, req.body, function(err, doc) {
        console.log('[[[err]]]');
        console.log(err);
        console.log('doc');
        console.log(doc);
       if (err) throw err;

       console.log('User saved successfully!');
       res.sendStatus(200)
     });

})